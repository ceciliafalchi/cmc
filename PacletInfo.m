(* ::Package:: *)

(* Paclet Info File *)

Paclet[
    Name -> "CMC",
    Version -> "1.0",
    MathematicaVersion -> "",
    Description -> "CMC for calculating ciruclarity mesaures",
    Extensions -> 
        {
            {"Documentation", Language->"English", LinkBase->"CMC",
            		Resources->{
				 "Guides/CMC"
 				}                
			}
        }
]
